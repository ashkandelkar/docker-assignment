FROM ubuntu
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=ASIA

RUN apt-get update 
RUN apt-get install tzdata -y 
RUN apt-get install apache2 -y 
